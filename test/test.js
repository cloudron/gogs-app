#!/usr/bin/env node

/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    superagent = require('superagent'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

// ensure private key permissions for ci
require('fs').chmodSync(__dirname + '/id_rsa', 0o600);

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 10000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };

    const repodir = '/tmp/testrepo';
    const reponame = 'testrepo';
    const username = process.env.USERNAME;
    const password = process.env.PASSWORD;
    const email = process.env.EMAIL;
    const adminUsername = 'root', adminPassword = 'changeme';

    let browser, app;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION || a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    }

    async function adminLogin(hasLdap) {
        await browser.manage().deleteAllCookies();
        await browser.get('https://' + app.fqdn + '/user/login');
        await browser.findElement(By.id('user_name')).sendKeys(adminUsername);
        await browser.findElement(By.id('password')).sendKeys(adminPassword);
        if (hasLdap) { // no selector combo with only one auth source
            await browser.findElement(By.xpath('//div[@class="ui selection dropdown"]')).sendKeys('l'); // Local
        }
        await browser.findElement(By.css('form')).submit();
        await browser.wait(until.elementLocated(By.linkText('Dashboard')), TIMEOUT);
    }

    async function login() {
        await browser.manage().deleteAllCookies();
        await browser.get('https://' + app.fqdn + '/user/login');
        await browser.findElement(By.id('user_name')).sendKeys(username);
        await browser.findElement(By.id('password')).sendKeys(password);
        await browser.findElement(By.tagName('form')).submit();
        await browser.wait(until.elementLocated(By.linkText('Dashboard')), TIMEOUT);
    }

    async function waitForUrl(url) {
        await browser.wait(async function () {
            const currentUrl = await browser.getCurrentUrl();
            return currentUrl === url;
        }, TIMEOUT);
    }

    async function logout() {
        await browser.get('https://' + app.fqdn);
        await browser.wait(until.elementLocated(By.xpath('//div[contains(@data-content, "User profile and more")]')), TIMEOUT);
        await browser.findElement(By.xpath('//div[contains(@data-content, "User profile and more")]')).click();
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//form[@id="logout-form"]')).submit();
        await waitForUrl('https://' + app.fqdn + '/explore/repos');
    }

    async function setAvatar() {
        await browser.get('https://' + app.fqdn + '/user/settings/avatar');
        await browser.findElement(By.xpath('//input[@type="file" and @name="avatar"]')).sendKeys(path.resolve(__dirname, '../logo.png'));
        await browser.findElement(By.xpath('//div/label[contains(text(), "Use Custom Avatar")]')).click();
        await browser.findElement(By.xpath('//button[contains(text(), "Update Avatar Setting")]')).click();
        await browser.wait(until.elementLocated(By.xpath('//p[contains(text(),"updated successfully")]')), TIMEOUT);
        await browser.sleep(10000);
    }

    async function checkAvatar() {
        const avatarId = '2';

        const response = await superagent.get('https://' + app.fqdn + '/avatars/' + avatarId);
        expect(response.statusCode).to.be(200);
    }

    async function createRepo() {
        await browser.get('https://' + app.fqdn);
        await browser.findElement(By.linkText('New Repository')).click();
        await browser.wait(until.elementLocated(By.xpath('//*[contains(text(), "New Repository")]')), TIMEOUT);
        await browser.findElement(By.id('repo_name')).sendKeys(reponame);
        const button = browser.findElement(By.xpath('//button[contains(text(), "Create Repository")]'));
        await browser.executeScript('arguments[0].scrollIntoView(true)', button);
        await browser.findElement(By.id('auto-init')).click();
        await browser.findElement(By.xpath('//button[contains(text(), "Create Repository")]')).click();
        await browser.wait(async function () {
            const url = await browser.getCurrentUrl();
            return url === 'https://' + app.fqdn + '/' + username + '/' + reponame;
        }, TIMEOUT);
    }

    function cloneRepo() {
        fs.rmSync(repodir, { recursive: true, force: true });
        const env = Object.create(process.env);
        env.GIT_SSH_COMMAND = __dirname + '/git_ssh_wrapper.sh';
        execSync('git clone ssh://git@' + app.fqdn + ':29418/' + username + '/' + reponame + '.git ' + repodir, { env: env });
    }

    function pushFile() {
        const env = Object.create(process.env);
        env.GIT_SSH_COMMAND = __dirname + '/git_ssh_wrapper.sh';
        execSync('touch newfile && git add newfile && git commit -a -mx && git push ssh://git@' + app.fqdn + ':29418/' + username + '/' + reponame + ' master', { env: env, cwd: repodir });
    }

    async function editFile() {
        await browser.get('https://' + app.fqdn + '/' + username + '/' + reponame + '/_edit/master/newfile');
        const cm = browser.findElement(By.xpath('//div[contains(@class,"CodeMirror")]'));
        const text = 'yo';
        await browser.executeScript('arguments[0].CodeMirror.setValue("' + text + '");', cm);
        await browser.findElement(By.xpath('//input[@name="commit_summary"]')).sendKeys('Dummy edit');
        await browser.findElement(By.xpath('//button[contains(text(), "Commit Changes")]')).click();
        await waitForUrl('https://' + app.fqdn + '/' + username + '/' + reponame + '/src/master/newfile');
    }

    function fileExists() {
        expect(fs.existsSync(repodir + '/newfile')).to.be(true);
    }

    async function checkCloneUrl() {
        await browser.get('https://' + app.fqdn + '/' + username + '/' + reponame);
        await browser.findElement(By.id('repo-clone-ssh')).click();
        const cloneUrl = await browser.findElement(By.id('repo-clone-url')).getAttribute('value');
        expect(cloneUrl).to.be('ssh://git@' + app.fqdn + ':29418/' + username + '/' + reponame + '.git');
    }

    async function addPublicKey() {
        const publicKey = fs.readFileSync(__dirname + '/id_rsa.pub', 'utf8');

        await browser.get('https://' + app.fqdn + '/user/settings/ssh');
        await browser.findElement(By.xpath('//div[text()="Add Key"]')).click();
        await browser.findElement(By.id('title')).sendKeys('testkey');
        await browser.findElement(By.id('content')).sendKeys(publicKey.trim()); // #3480
        var button = browser.findElement(By.xpath('//button[contains(text(), "Add Key")]'));
        await browser.executeScript('arguments[0].scrollIntoView(false)', button);
        await browser.findElement(By.xpath('//button[contains(text(), "Add Key")]')).click();
        await browser.wait(until.elementLocated(By.xpath('//p[contains(text(), "added successfully!")]')), TIMEOUT);
    }

    async function sendMail() {
        await browser.get('https://' + app.fqdn + '/admin/config');
        const button = browser.findElement(By.xpath('//button[@id="test-mail-btn"]'));
        await browser.executeScript('arguments[0].scrollIntoView(true)', button);
        await browser.findElement(By.xpath('//input[@name="email"]')).sendKeys('test@cloudron.io');
        await browser.findElement(By.xpath('//button[@id="test-mail-btn"]')).click();
        await browser.wait(until.elementLocated(By.xpath('//p[contains(text(),"Test email has been sent to \'test@cloudron.io\'")]')), TIMEOUT);
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync('cloudron install --location ' + LOCATION, EXEC_ARGS); });
    it('can get app information', getAppInfo);

    it('can admin login', adminLogin.bind(null, true));
    it('can send mail', sendMail);
    it('can logout', logout);

    it('can login', login);
    it('can set avatar', setAvatar);
    it('can get avatar', checkAvatar);
    it('can add public key', addPublicKey);
    it('can create repo', createRepo);
    it('displays correct clone url', checkCloneUrl);
    it('can clone the url', cloneRepo);
    it('can add and push a file', pushFile);
    it('can edit file', editFile);

    it('can restart app', function () { execSync('cloudron restart --app ' + app.id); });

    it('can clone the url', cloneRepo);
    it('file exists in cloned repo', fileExists);

    it('backup app', function () { execSync('cloudron backup create --app ' + app.id, EXEC_ARGS); });

    it('restore app', function () {
        const backups = JSON.parse(execSync('cloudron backup list --raw'));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can get avatar', checkAvatar);
    it('can clone the url', cloneRepo);
    it('file exists in cloned repo', fileExists);

    it('move to different location', function () { execSync('cloudron configure --location ' + LOCATION + '2 --app ' + app.id, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can login', login);
    it('can get avatar', checkAvatar);
    it('displays correct clone url', checkCloneUrl);
    it('can clone the url', cloneRepo);
    it('file exists in cloned repo', fileExists);

    it('uninstall app', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });

    // No SSO
    it('install app (no sso)', function () { execSync('cloudron install --no-sso --location ' + LOCATION, EXEC_ARGS); });
    it('can get app information', getAppInfo);

    it('can admin login (no sso)', adminLogin.bind(null, false));
    it('can logout', logout);

    it('uninstall app (no sso)', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });

    // test update
    it('can install app', function () { execSync('cloudron install --appstore-id ' + app.manifest.id + ' --location ' + LOCATION, EXEC_ARGS); });
    it('can get app information', getAppInfo);

    it('can login', login);
    it('can set avatar', setAvatar);
    it('can get avatar', checkAvatar);
    it('can add public key', addPublicKey);
    it('can create repo', createRepo);
    it('can clone the url', cloneRepo);
    it('can add and push a file', pushFile);
    it('can logout', logout);

    it('can update', function () { execSync('cloudron update --app ' + app.id, EXEC_ARGS); });

    it('can admin login', adminLogin.bind(null, true));
    it('can send mail', sendMail);
    it('can logout', logout);

    it('can login', login);
    it('can get avatar', checkAvatar);
    it('can clone the url', cloneRepo);
    it('file exists in cloned repo', fileExists);

    it('uninstall app', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });
});
