# Gogs Cloudron App

This repository contains the Cloudron app package source for [Gogs](http://gogs.io/).

## Installation

[![Install](https://cloudron.io/img/button.svg)](https://cloudron.io/button.html?app=io.gogs.cloudronapp)

or using the [Cloudron command line tooling](https://cloudron.io/references/cli.html)

```
cloudron install --appstore-id io.gogs.cloudronapp
```

## Building

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
cd gogs-app

cloudron build
cloudron install
```

## Testing

The e2e tests are located in the `test/` folder and require [nodejs](http://nodejs.org/). They are creating a fresh build, install the app on your Cloudron, perform tests, backup, restore and test if the repos are still ok. The tests expect port 29418 to be available.

```
cd gogs-app/test

npm install
EMAIL=<cloudron user email> USERNAME=<cloudron username> PASSWORD=<cloudron password> mocha --bail test.js
```

## Compiling from source

export GOPATH=/home/git/gows
mkdir -p ${GOPATH}/gows 
mkdir -p ${GOPATH}/src/github.com/gogits
cd ${GOPATH}/src/github.com/gogits
git clone https://github.com/gogits/gogs.git
cd gogs
go get ./...
go build
go install 

