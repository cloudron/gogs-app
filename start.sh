#!/bin/bash

set -eu -o pipefail

mkdir -p /run/gogs/tmp/uploads /run/sshd /app/data/custom

setup_ldap_source() {
    set -eu

    # Get the existing LDAP source status. This allows the user to disable LDAP
    # Note that this method is deprecated since this app now supports optionalSso
    ldap_status=$(mysql -u"${CLOUDRON_MYSQL_USERNAME}" -p"${CLOUDRON_MYSQL_PASSWORD}" -h mysql --database="${CLOUDRON_MYSQL_DATABASE}" -N -B -e "select is_actived from login_source WHERE name='cloudron';")
    [[ -z "${ldap_status}" ]] && ldap_status="1"

    now=$(date +%s)

    if mysql -u"${CLOUDRON_MYSQL_USERNAME}" -p"${CLOUDRON_MYSQL_PASSWORD}" -h mysql --database="${CLOUDRON_MYSQL_DATABASE}" \
        -e "REPLACE INTO login_source (id, type, name, is_actived, is_default, cfg, created_unix, updated_unix) VALUES (1,2,'cloudron',${ldap_status},${ldap_status},'{\"Name\":\"Cloudron\",\"Host\":\"${CLOUDRON_LDAP_SERVER}\",\"Port\":${CLOUDRON_LDAP_PORT},\"UseSSL\":false,\"SkipVerify\":true,\"BindDN\":\"${CLOUDRON_LDAP_BIND_DN}\",\"BindPassword\":\"${CLOUDRON_LDAP_BIND_PASSWORD}\",\"UserBase\":\"${CLOUDRON_LDAP_USERS_BASE_DN}\",\"AttributeUsername\":\"username\",\"AttributeName\":\"displayname\",\"AttributeSurname\":\"\",\"AttributeMail\":\"mail\",\"Filter\":\"(\\\\u007C(mail=%s)(username=%s))\"}','${now}','${now}');"; then
        echo "==> LDAP Authentication was setup with ldap activation status ${ldap_status}"
    else
        echo "==> Failed to setup LDAP authentication"
        exit 1
    fi
}

setup_root_user() {
    set -eu

    if sudo -H -u git /home/git/gogs/gogs admin create-user --name root --password changeme --email admin@cloudron.local --admin -c /run/gogs/app.ini; then
        echo "==> root user added"
    else
        echo "==> Failed to add root user"
        exit 1
    fi
}

setup_auth() {
    set -eu

    # Wait for gogs to finish db setup, before we do any db operations
    while ! curl --fail http://localhost:3000/healthcheck; do
        echo "==> Waiting for gogs to come up"
        sleep 1
    done

    echo "==> Gogs is up, setting up auth"

    if [[ -n "${CLOUDRON_LDAP_SERVER:-}" ]]; then
        setup_ldap_source
    fi

    user_count=$(mysql -u"${CLOUDRON_MYSQL_USERNAME}" -p"${CLOUDRON_MYSQL_PASSWORD}" -h mysql --database="${CLOUDRON_MYSQL_DATABASE}" -N -B -e "SELECT count(*) FROM user;")
    # be careful, not to create root user for existing LDAP based installs
    if [[ "${user_count}" == "0" ]]; then
        echo "==> Setting up root user for first run"
        setup_root_user
    fi
}

# SSH_PORT can be unset to disable SSH
disable_ssh="false"
if [[ -z "${SSH_PORT:-}" ]]; then
    echo "==> SSH disabled"
    SSH_PORT=29418 # arbitrary port to keep sshd happy
    disable_ssh="true"
fi

if [[ ! -f "/app/data/sshd/ssh_host_ed25519_key" ]]; then
    echo "==> Generating ssh host keys"
    mkdir -p /app/data/sshd
    ssh-keygen -qt rsa -N '' -f /app/data/sshd/ssh_host_rsa_key
    ssh-keygen -qt dsa -N '' -f /app/data/sshd/ssh_host_dsa_key
    ssh-keygen -qt ecdsa -N '' -f /app/data/sshd/ssh_host_ecdsa_key
    ssh-keygen -qt ed25519 -N '' -f /app/data/sshd/ssh_host_ed25519_key
else
    echo "==> Reusing existing host keys"
fi

chmod 0600 /app/data/sshd/*_key
chmod 0644 /app/data/sshd/*.pub

# generate ssh config
sed -e "s/^Port .*/Port ${SSH_PORT}/" /etc/ssh/sshd_config > /run/gogs/sshd_config

if [[ ! -f /app/data/app.ini ]]; then
    echo -e "; Add customizations here - https://gogs.io/docs/advanced/configuration_cheat_sheet" > /app/data/app.ini

    echo "==> Generating new SECRET_KEY"
    crudini --set "/app/data/app.ini" security SECRET_KEY $(pwgen -1 -s)
fi

# merge user config file
cp /home/git/app.ini.template "/run/gogs/app.ini"
crudini --merge "/run/gogs/app.ini" < "/app/data/app.ini"

# override important values
crudini --set "/run/gogs/app.ini" database TYPE mysql
crudini --set "/run/gogs/app.ini" database HOST "${CLOUDRON_MYSQL_HOST}:${CLOUDRON_MYSQL_PORT}"
crudini --set "/run/gogs/app.ini" database NAME "${CLOUDRON_MYSQL_DATABASE}"
crudini --set "/run/gogs/app.ini" database USER "${CLOUDRON_MYSQL_USERNAME}"
crudini --set "/run/gogs/app.ini" database PASSWORD "${CLOUDRON_MYSQL_PASSWORD}"
crudini --set "/run/gogs/app.ini" database SSL_MODE "disable"
crudini --set "/run/gogs/app.ini" server PROTOCOL "http"
crudini --set "/run/gogs/app.ini" server DOMAIN "${CLOUDRON_APP_DOMAIN}"
crudini --set "/run/gogs/app.ini" server ROOT_URL "https://%(DOMAIN)s/"
crudini --set "/run/gogs/app.ini" server HTTP_ADDR ""
crudini --set "/run/gogs/app.ini" server HTTP_PORT "3000"
crudini --set "/run/gogs/app.ini" server DISABLE_SSH "${disable_ssh}"
crudini --set "/run/gogs/app.ini" server SSH_PORT "${SSH_PORT}"
crudini --set "/run/gogs/app.ini" server APP_DATA_PATH "/app/data/appdata"
crudini --set "/run/gogs/app.ini" repository ROOT "/app/data/repository"
crudini --set "/run/gogs/app.ini" repository.upload TEMP_PATH "/run/gogs/tmp/uploads"
# note that gogs relies SMTPS_PORT ending with 465 to determine SMTPS
crudini --set "/run/gogs/app.ini" email HOST "${CLOUDRON_MAIL_SMTP_SERVER}:${CLOUDRON_MAIL_SMTPS_PORT}"
crudini --set "/run/gogs/app.ini" email USER "${CLOUDRON_MAIL_SMTP_USERNAME}"
crudini --set "/run/gogs/app.ini" email PASSWORD "${CLOUDRON_MAIL_SMTP_PASSWORD}"
crudini --set "/run/gogs/app.ini" email FROM "${CLOUDRON_MAIL_FROM_DISPLAY_NAME:-Gogs} <${CLOUDRON_MAIL_FROM}>"
crudini --set "/run/gogs/app.ini" email SKIP_VERIFY "true"
crudini --set "/run/gogs/app.ini" security INSTALL_LOCK "true"
crudini --set "/run/gogs/app.ini" log MODE "console"
crudini --set "/run/gogs/app.ini" log ROOT_PATH "/run/gogs"

echo "==> Creating dirs and changing permissions"
mkdir -p /app/data/repository /app/data/ssh
chown -R git:git /app/data /run/gogs

# this expects app.ini to be available
( setup_auth ) &

echo "==> Starting Gogs"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i Gogs

