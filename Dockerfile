FROM cloudron/base:5.0.0@sha256:04fd70dbd8ad6149c19de39e35718e024417c3e01dc9c6637eaf4a41ec4e596c

RUN apt-get update && \
    apt-get install -y openssh-server git && \
    rm -rf /etc/ssh_host_* && \
    rm -r /var/cache/apt /var/lib/apt/lists

ADD supervisor/ /etc/supervisor/conf.d/

RUN useradd --comment "Gogs" --create-home --shell /bin/bash git
RUN passwd -d git # by default, git account is created as inactive which prevents login via openssh. this disables password for account
WORKDIR /home/git

# renovate: datasource=github-releases depName=gogs/gogs versioning=semver extractVersion=^v(?<version>.+)$
ARG GOGS_VERSION=0.13.2

RUN mkdir -p /home/git/gogs
RUN cd /home/git/gogs && \
    curl -L https://github.com/gogs/gogs/releases/download/v${GOGS_VERSION}/gogs_${GOGS_VERSION}_linux_amd64.tar.gz | tar zxvf - --strip-components 1

# setup config paths
ADD app.ini.template /home/git/app.ini.template

# setup log paths
RUN mkdir -p /run/gogs && chown -R git:git /run/gogs
RUN sed -e 's,^logfile=.*$,logfile=/run/gogs/supervisord.log,' -i /etc/supervisor/supervisord.conf

RUN ln -s /app/data/ssh /home/git/.ssh
RUN ln -s /app/data/gitconfig /home/git/.gitconfig

ADD start.sh /home/git/start.sh
ADD sshd_config /etc/ssh/sshd_config

CMD [ "/home/git/start.sh" ]
