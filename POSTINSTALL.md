This app is pre-setup with an admin account (use the `Local` authentication source for logging in as admin).
The initial credentials are:
  
**Username**: root<br/>
**Password**: changeme<br/>

Please change the admin password immediately.

<sso>
Cloudron users can login using the `Cloudron` authentication source.
</sso>

